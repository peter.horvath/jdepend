package jdepend.framework;

import java.util.LinkedList;
import java.util.List;

import jdepend.jsonui.JsonUI;
import jdepend.swingui.SwingUI;
import jdepend.textui.TextUI;
import jdepend.xmlui.XmlUI;

public class Main {
	private static void uiUsage() {
		System.err.println("Usage: <-jsonui|-textui|-xmlui|-swingui> -help");
	}

	public static void main(String args[]) {
		List<String> uiArgs = new LinkedList<String>();
		JdependUI ui = null;

		for (String arg : args) {
			if (arg.equals("-jsonui") && ui == null) {
				ui = new JsonUI();
			} else if (arg.equals("-textui") && ui == null) {
				ui = new TextUI();
			} else if (arg.equals("-xmlui") && ui == null) {
				ui = new XmlUI();
			} else if (arg.equals("-swingui") && ui == null) {
				ui = new SwingUI();
			} else {
				uiArgs.add(arg);
			}
		}

		if (ui == null) {
			uiUsage();
		} else {
			ui.instanceMain(uiArgs.toArray(new String[uiArgs.size()]));
		}
	}
}