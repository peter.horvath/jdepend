package jdepend.framework;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The <code>JavaClass</code> class represents a Java
 * class or interface.
 *
 * @author <b>Mike Clark</b>
 * @author Clarkware Consulting, Inc.
 */

public class JavaClass {

    private String className;
    private String packageName;
    private boolean isAbstract;
    private Map<String, JavaPackage> imports;
    private Map<String, String> info;
    private String sourceFile;
    private List<String> methodInfo;
    private List<String> fieldInfo;

    public JavaClass(String name) {
        className = name;
        packageName = "default";
        isAbstract = false;
        imports = new HashMap<String, JavaPackage>();
        info = new HashMap<String, String>();
        methodInfo = new ArrayList<String>();
        fieldInfo = new ArrayList<String>();
        sourceFile = "Unknown";
    }

    public void setName(String name) {
        className = name;
    }

    public String getName() {
        return className;
    }

    public void setPackageName(String name) {
        packageName = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setSourceFile(String name) {
        sourceFile = name;
    }

    public String getSourceFile() {
        return sourceFile;
    }

    public Collection<JavaPackage> getImportedPackages() {
        return imports.values();
    }

    public void addImportedPackage(JavaPackage jPackage) {
        if (!jPackage.getName().equals(getPackageName())) {
            imports.put(jPackage.getName(), jPackage);
        }
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public void isAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    public boolean equals(Object other) {
        if (other instanceof JavaClass) {
            JavaClass otherClass = (JavaClass) other;
            return otherClass.getName().equals(getName());
        }

        return false;
    }

    public int hashCode() {
        return getName().hashCode();
    }

    public static class ClassComparator implements Comparator<JavaClass> {
        public int compare(JavaClass a, JavaClass b) {
            return a.getName().compareTo(b.getName());
        }
    }

	public void addMethodInfo(String info) {
		this.methodInfo.add(info);
	}
	public void addFieldInfo(String info) {
		this.fieldInfo.add(info);
	}
	
	public List<String> getMethodInfo() {
		return methodInfo;
	}

	public List<String> getFieldInfo() {
		return fieldInfo;
	}

	public void addClassInfo(String key, String value) {
		this.info.put(key, value);
	}
	public Map<String, String> getClassExtraInfo() {
		return this.info;
	}

}
