package jdepend.framework.rule;

/**
 *
 */
public interface RuleDefiner {
	void defineRules();
}