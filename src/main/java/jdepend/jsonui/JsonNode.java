package jdepend.jsonui;

import java.util.LinkedList;
import java.util.List;

class JsonNode extends JsonEntity {
	private long idn;
	private String id;
	private String name;
	private String type;
	private String status;
	private long group;
	private List<String> cycles = new LinkedList<String>();

	public JsonNode(String name, String type, String status) {
		this.id = name;
		this.name = name;
		this.type = type;
		this.status = status;
	}

	public String getName() {
		return this.id;
	}
	
	public void addCycleMember(String pkgName) {
		this.cycles.add(pkgName);
	}
	public void setIdn(long id){
		this.idn = id;
	}

	public void setGroup(long g) {
		this.group = g;
	}

	public long getGroup() {
		return group;
	}
}