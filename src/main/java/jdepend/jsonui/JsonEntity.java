package jdepend.jsonui;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

class JsonEntity {
	private Map<String, String> stats = new TreeMap<String, String>();
	private List<String> warnings = new LinkedList<String>();
	
	public void setStat(String name, Float value) {
		this.stats.put(name, value.toString());
	}

	public void setStat(String name, int value) {
		this.setStat(name, new Float(value));
	}
	
	public void setStat(String name, String value) {
		this.stats.put(name, value);
	}

	public void addWarning(String warning) {
		this.warnings.add(warning);
	}
}