package jdepend.jsonui;

import java.util.List;

public class JsonClassNode extends JsonNode {

	private String packagename;
	private String classname;
    
	private List<String> methods;
    private List<String> fields;
    
	public JsonClassNode(String name, String type, String status) {
		super(name, type, status);
	}
	
	public JsonClassNode(String name, String packagename, String type, String status) {
		super(name, type, status);
		this.packagename = packagename;
		setClassName(name);
	}
	
	private void setClassName(String name){
		String[] split = name.split("\\.");
		this.classname = split[split.length-1];
	}

	public void setMethods(List<String> methods) {
		this.methods = methods;
	}

	public void setFields(List<String> fields) {
		this.fields = fields;
	}
	

}
