package jdepend.jsonui;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jdepend.framework.ClassFileParser;
import jdepend.framework.JDepend;
import jdepend.framework.JavaClass;
import jdepend.framework.JavaClassBuilder;
import jdepend.framework.JavaPackage;
import jdepend.framework.JdependUI;
import jdepend.framework.PackageComparator;
import jdepend.framework.PackageFilter;
import jdepend.framework.ParserListener;

/**
 * The <code>JDepend</code> class analyzes directories of Java class files,
 * generates metrics for each Java package, and reports the metrics in JSON
 * format.
 *
 * @author <b>Peter Horvath</b>
 * @author Isento GmbH, http://isento.de
 */

public class JsonUI implements JdependUI {
	private JDepend analyzer;
	private PrintWriter writer;
	private JsonResult result = new JsonResult();
	long groups = 0 ;
	/**
	 * Sets the output writer.
	 *
	 * @param writer
	 *            Output writer.
	 */
	public void setWriter(PrintWriter writer) {
		this.writer = writer;
	}

	protected PrintWriter getWriter() {
		return writer;
	}

	/**
	 * Constructs a <code>JDepend</code> instance using standard output.
	 */
	public JsonUI() {
		initialize(new PrintWriter(System.out));
	}

	/**
	 * Constructs a <code>JDepend</code> instance with the specified writer.
	 *
	 * @param writer
	 *            Writer.
	 */
	public JsonUI(PrintWriter writer) {
		initialize(writer);
	}

	public void initialize(PrintWriter writer) {
		analyzer = new JDepend(PackageFilter.all().excludingProperties());
		analyzeInnerClasses(false);
		setWriter(writer);
	}

	protected void usage(String message) {
		if (message != null) {
			System.err.println("\n" + message);
		}
		String baseUsage = "\nJDepend -jsonui ";

		System.err.println("");
		System.err.println("usage: ");
		System.err.println(baseUsage + "[-components <components>] [-minimized] [-file <output file>] <directory> "
				+ "[directory2 [directory 3] ...]");
		System.exit(1);
	}

	/**
	 * Main.
	 */
	public void instanceMain(String args[]) {
		int directoryCount = 0;
		boolean minimized = false;

		if (args.length < 1) {
			usage("Must specify at least one directory.");
		}

		for (int i = 0; i < args.length; i++) {
			if (args[i].startsWith("-")) {
				if (args[i].equalsIgnoreCase("-file")) {

					if (args.length <= i + 1) {
						usage("Output file name not specified.");
					}

					try {
						setWriter(new PrintWriter(new OutputStreamWriter(new FileOutputStream(args[++i]), "UTF8")));
					} catch (IOException ioe) {
						usage(ioe.getMessage());
					}

				} else if (args[i].equalsIgnoreCase("-components")) {
					if (args.length <= i + 1) {
						usage("Components not specified.");
					}
					setComponents(args[++i]);
				} else if (args[i].equalsIgnoreCase("-minimized")) {
					minimized = true;
				} else {
					usage("Invalid argument: " + args[i]);
				}
			} else {
				try {
					addDirectory(args[i]);
					directoryCount++;
				} catch (IOException ioe) {
					usage("Directory does not exist: " + args[i]);
				}
			}
		}

		if (directoryCount == 0) {
			usage("Must specify at least one directory.");
		}

		analyze();
		getWriter().print(this.result.toString(!minimized));
		getWriter().flush();
	}

	/**
	 * 
	 * @param directories,
	 *            directory list
	 * @param components,
	 *            component List
	 * @param writer,
	 *            default: new PrintWriter(System.out)
	 * @param minified,
	 *            boolean
	 * @throws JsonUIException 
	 */
	public void runAnalyze(List<String> directories, List<String> components, PrintWriter writer,  boolean minified) throws JsonUIException {

		if (directories.size() == 0) {
			throw new JsonUIException("Must specify at least one directory.");
		}
		if (writer == null) {
			writer = new PrintWriter(System.out);
		} else {
			setWriter(writer);
		}
		addDirectory(directories);
		setComponents(components);
		
		analyze();
		getWriter().print(this.result.toString(!minified));
		getWriter().flush();
	}
	
	/**
	 * 
	 * @param directories,
	 *            directory list
	 * @return JsonString result
	 * @throws JsonUIException 
	 */
	public String analyzeToString(List<String> directories) throws JsonUIException {
		return analyzeToString(directories, new ArrayList<String>());
	}
	
	/**
	 * 
	 * @param directories,
	 *            directory list
	 * @param components,
	 *            component list
	 * @return JsonString result
	 * @throws JsonUIException 
	 */
	public String analyzeToString(List<String> directories, List<String> components) throws JsonUIException {
		StringWriter strOut = new StringWriter();
		PrintWriter writer = new PrintWriter(strOut);
		runAnalyze(directories, components, writer,  true);		
		return strOut.toString();
	}
	
	/**
	 * sets the Writer to a File with name {@code filename}
	 * @param filename, 
	 * 					File name for the Output File
	 * @throws UnsupportedEncodingException, UTF8 not supported
	 * @throws FileNotFoundException
	 * 
	 * */
	public void setWriterToFile(String filename) throws UnsupportedEncodingException, FileNotFoundException{
		setWriter(new PrintWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF8")));
	}

	/**
	 * Analyzes the registered directories, generates metrics for each Java
	 * package, and reports the metrics.
	 */
	public void analyze() {
		List<JavaPackage> packageList = new ArrayList<JavaPackage>(analyzer.analyze());

		Collections.sort(packageList, new PackageComparator(PackageComparator.byName()));

		processPackages(packageList);
	}

	protected void processPackages(Collection<JavaPackage> packages) {
		
		for (JavaPackage aPackage : packages) {
			this.groups ++ ;
			processPackage(aPackage);
		}
	}

	protected void processPackage(JavaPackage jPackage) {
		String pkgName = jPackage.getName();
		String pkgType = "package";
		String pkgStatus = "ok";
		
		JsonNode node = new JsonNode(pkgName, pkgType, pkgStatus);
		node.setGroup(this.groups);
		if (jPackage.getClasses().size() == 0) {
			node.addWarning("No stats available: package referenced, but not analyzed.");
			return;
		}

		processPackageStatistics(node, jPackage);

		processPackageClasses(node, jPackage);

		processPackageEfferents(node, jPackage);

		// processPackageAfferents(node, jPackage);

		processPackageCycle(node, jPackage);

		this.result.addNode(node);
	}

	protected void processPackageStatistics(JsonNode node, JavaPackage jPackage) {
		node.setStat("TotalClasses", jPackage.getClassCount());
		node.setStat("ConcreteClasses", jPackage.getConcreteClassCount());
		node.setStat("AbstractClasses", jPackage.getAbstractClassCount());
		node.setStat("AfferentCoupling", jPackage.afferentCoupling());
		node.setStat("EfferentCoupling", jPackage.efferentCoupling());
		node.setStat("Abstractness", jPackage.abstractness());
		node.setStat("Instability", jPackage.instability());
		node.setStat("Distance", jPackage.distance());
		node.setStat("Volatility", jPackage.getVolatility());
	}

	protected void processPackageClasses(JsonNode node, JavaPackage jPackage) {
		List<JavaClass> members = new ArrayList<JavaClass>(jPackage.getClasses());
		Collections.sort(members, new JavaClass.ClassComparator());
		for (JavaClass jClass : members) {
			processPackageClass(node, jClass);
			jPackage.addClass(jClass);
		}
	}

	protected void processPackageClass(JsonNode pkgNode, JavaClass jClass) {
		String className = jClass.getName();
		String packageName = jClass.getPackageName();
		String classType = jClass.isAbstract() ? "AbstractClass" : "ConcreteClass";
		String classStatus = "ok";
		JsonClassNode classNode = new JsonClassNode(className,packageName, classType, classStatus);
		classNode.setGroup(pkgNode.getGroup());
		classNode.setStat("SourceFile", jClass.getSourceFile());
		classNode.setFields(jClass.getFieldInfo());
		classNode.setMethods(jClass.getMethodInfo());
		if(!jClass.getClassExtraInfo().isEmpty()){
			for(Entry<String, String> infos : jClass.getClassExtraInfo().entrySet())
			{
				classNode.setStat(infos.getKey(), infos.getValue());
			}
		}
		this.result.addNode(classNode);

		JsonLink parentLink = new JsonLink(className, pkgNode.getName(), "classMemberOfPackage");
		this.result.addLink(parentLink);
	}

	protected void processPackageEfferents(JsonNode pkgNode, JavaPackage jPackage) {
		List<JavaPackage> efferents = new ArrayList<JavaPackage>(jPackage.getEfferents());
		Collections.sort(efferents, new PackageComparator(PackageComparator.byName()));
		for (JavaPackage efferent : efferents) {
			JsonLink link = new JsonLink(pkgNode.getName(), efferent.getName(), "EfferentCoupling");
			this.result.addLink(link);
		}
		if (efferents.size() == 0) {
			pkgNode.addWarning("Not dependent on any packages.");
		}
	}

	protected void processPackageAfferents(JsonNode pkgNode, JavaPackage jPackage) {
		List<JavaPackage> afferents = new ArrayList<JavaPackage>(jPackage.getAfferents());
		Collections.sort(afferents, new PackageComparator(PackageComparator.byName()));
		for (JavaPackage afferent : afferents) {
			JsonLink link = new JsonLink(pkgNode.getName(), afferent.getName(), "AfferentCoupling");
			this.result.addLink(link);
		}
		if (afferents.size() == 0) {
			pkgNode.addWarning("Not used by any packages.");
		}
	}

	/**
	 * TODO: cycles optimize.
	 * */
	protected void processPackageCycle(JsonNode pkgNode, JavaPackage jPackage) {
		List<JavaPackage> list = new ArrayList<JavaPackage>();
		jPackage.collectCycle(list);

		if (!jPackage.containsCycle()) {
			return;
		}

		JavaPackage cyclePackage = list.get(list.size() - 1);
		String cyclePkgName = cyclePackage.getName();
		pkgNode.setStat("cycleTarget", cyclePkgName);

		for (JavaPackage pkg : list) {
			pkgNode.addCycleMember(pkg.getName());
		}
	}

	/**
	 * Sets the package filter.
	 *
	 * @param filter
	 *            Package filter.
	 */
	public void setFilter(PackageFilter filter) {
		analyzer.setFilter(filter);
	}

	/**
	 * Sets the comma-separated list of components.
	 */
	public void setComponents(String components) {
		analyzer.setComponents(components);
	}

	/**
	 * @param components
	 *            as String list
	 */
	public void setComponents(List<String> components) {
		for (String component : components) {
			analyzer.setComponents(component);
		}
	}

	/**
	 * Adds the specified directory name to the collection of directories to be
	 * analyzed.
	 *
	 * @param name
	 *            Directory name.
	 * @throws IOException
	 *             If the directory does not exist.
	 */
	public void addDirectory(String name) throws IOException {
		analyzer.addDirectory(name);
	}

	/**
	 * Adds the specified directory name to the collection of directories to be
	 * analyzed.
	 *
	 * @param directories
	 *            Directory List.
	 * @throws JsonUIException
	 *             If the directory does not exist.
	 */
	public void addDirectory(List<String> directories) throws JsonUIException {
		for (String directory : directories) {
			try {
				analyzer.addDirectory(directory);
			} catch (IOException e) {
				throw new JsonUIException("Directory does not exist: " + directory);
			}
		}
	}
	
	public void analyzeInnerClasses(boolean b){
		analyzer.analyzeInnerClasses(b);
	}
}