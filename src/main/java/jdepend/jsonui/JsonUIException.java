package jdepend.jsonui;

public class JsonUIException extends Exception {

	private static final long serialVersionUID = 1L;

	public JsonUIException(String message) {
        super(message);
    }
	
}
