package jdepend.jsonui;

class JsonLink extends JsonEntity {
	private long id ;
	private String source;
	private String target;
	private String type;

	public JsonLink(String source, String target, String type) {
		this.source = source;
		this.target = target;
		this.type = type;
	}
	public void setId(long id){
		this.id = id;
	}
}