package jdepend.jsonui;

import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

class JsonResult extends JsonEntity {
	private List<JsonNode> nodes = new LinkedList<JsonNode>();
	private List<JsonLink> links = new LinkedList<JsonLink>();
	private long nodeCounter = 0;
	private long linkCounter = 0;
	public void addNode(JsonNode node) {
		node.setIdn(nodeCounter++);
		this.nodes.add(node);
	}

	public void addLink(JsonLink link) {
		link.setId(linkCounter++);
		this.links.add(link);
	}

	public String toString(boolean prettyPrint) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		if (prettyPrint) {
			gsonBuilder.setPrettyPrinting();
		}
		Gson gson = gsonBuilder.create();
		return gson.toJson(this);
	}
	
	public String toString() {
		return this.toString(false);
	}
}