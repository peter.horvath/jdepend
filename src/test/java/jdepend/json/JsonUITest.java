package jdepend.json;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import jdepend.jsonui.JsonUI;
import jdepend.jsonui.JsonUIException;

public class JsonUITest  {
    private String homeDir;
    private String testDir;
    private String testDataDir;
    private String buildDir;
    private String testBuildDir;
    private String packageSubDir;
    private String originalUserHome;

    @Before
    public void start() {
        System.setProperty("jdepend.home", ".");

        homeDir = System.getProperty("jdepend.home");
        if (homeDir == null) {
            fail("Property 'jdepend.home' not defined");
        }
        homeDir = homeDir + File.separator;
        testDir = homeDir + File.separator + "src" + File.separator + "test" + File.separator;
        testDataDir = testDir + File.separator;
        buildDir = homeDir + "target/classes" + File.separator;
        testBuildDir = homeDir + "target/test-classes" + File.separator;
        packageSubDir = "jdepend" + File.separator + "framework" + File.separator;
        originalUserHome = System.getProperty("user.home");
    }

    @After
    public void end() {
        System.setProperty("user.home", originalUserHome);
    }
    
    @Test
	public void analyzeToString() throws JsonUIException{
		List<String> directories = new ArrayList<String>();
		directories.add(testDir);
		JsonUI ui = new JsonUI();
        String result = ui.analyzeToString(directories);
        System.out.println(result);
	}

}
